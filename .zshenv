# Je ne suis pas certain à quoi sert cette ligne...
#. "$HOME/.cargo/env"

# J'ai enlevé $home/.cargo/bin car il est déjà dans path somehow
# Ancienne ligne: PATH="$PATH:$HOME/.local/bin:$HOME/.myscripts:$HOME/.cargo/bin"
PATH="$PATH:$HOME/.local/bin:$HOME/.myscripts"
PATH="$PATH:/usr/local/go/bin/"
PWD_COLOR=#83a598
STATUS_COLOR=#282828
CLEAN_COLOR=#b8bb26
DIRTY_COLOR=#fabd2f
