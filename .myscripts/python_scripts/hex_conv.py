def hex_to_dec(hex_num):
    return int(hex_num, 16)

def dec_to_hex(dec_num):
    return hex(dec_num)

if __name__ == '__main__':
    while True:
        print("Select conversion type:")
        print("1. Hex to Dec")
        print("2. Dec to Hex")
        print("3. Quit")
        choice = int(input("Enter choice: "))
    
        if choice == 1:
            hex_num = input("Enter hex number: ")
            print("Decimal equivalent:", hex_to_dec(hex_num))
        elif choice == 2:
            dec_num = int(input("Enter decimal number: "))
            print("Hexadecimal equivalent:", dec_to_hex(dec_num))
        elif choice == 3:
            break
        else:
            print("Invalid input")

