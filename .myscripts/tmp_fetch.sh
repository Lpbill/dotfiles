#!/bin/bash

# Set up the source and destination directories
src_dir="/tmp"
dest_dir="/home/lpbilodeau/tmp/"

# Watch for changes in the source directory
inotifywait -m -e modify $src_dir | while read path action file; do
    # Check if the modified file has a .s extension
    if [[ "$file" =~ \.s$ ]]; then
        # Copy the file to the destination directory
        cp $src_dir/$file $dest_dir
    fi
done
